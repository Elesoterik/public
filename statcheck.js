const EventEmitter = require('events');
class EE extends EventEmitter{}
const rankcheck = new EE();
const gm = require('gm');
const fs = require('fs');
const https = require('https');
const urlencode = require('urlencode');
const riotapi = "riotapitoken";
const regions = ['na', 'oce', 'euw', 'eune', 'las', 'lan', 'br', 'kr']
rankcheck.name="LOLStats";

rankcheck.on("load",(p,data)=>{
	//console.log(riotapi);
});
rankcheck.on("unload",(p,data)=>{
	//console.log("rankcheck unloaded!");
});

function getSummID(region, summname, summid)
{
	https.get('https://'+region+'.api.pvp.net/api/lol/'+region+'/v1.4/summoner/by-name/'+urlencode(summname)+'?api_key='+riotapi, function (res) {
		res.setEncoding('utf8');
		if (res.statusCode < 200 || res.statusCode > 299) { return; } else {
	    	res.on('data', function (chunk) {
	    		if (chunk.indexOf("<") != -1) {console.log("NO"); return;}
	    		sid = JSON.parse(chunk);
	    	});
	    	res.on('end', function () {
	    		summid && summid(sid[summname].id);
	    	});
	    }
	});
}

function getSummStats(region, summid, type, summdata)
{
	https.get('https://'+region+'.api.pvp.net/api/lol/'+region+'/v2.5/league/by-summoner/'+summid+'/entry?api_key='+riotapi, function (res) {
		var body = '';
		res.setEncoding('utf8');
		if (res.statusCode < 200 || res.statusCode > 299) { return; } else {
			res.on('data', function (chunk) {
				body = body + chunk;	
			});
			res.on('end', function () {
				try {
					var data = JSON.parse(body);
					switch(type){
						case "RANKED_SOLO_5x5":
							for(i=0;i<data[summid].length;i++){
								var rGuild = data[summid][i].name;
								var rTier = data[summid][i].tier;
								var rDIV = data[summid][i].entries[0].division;
								var rLP = data[summid][i].entries[0].leaguePoints;
								var rWins = data[summid][i].entries[0].wins;
								var rLosses = data[summid][i].entries[0].losses;
								var rStreak = data[summid][i].entries[0].isHotStreak;
								var rVeteran = data[summid][i].entries[0].isVeteran;
								var rBlood = data[summid][i].entries[0].isFreshBlood;
								var rInactive = data[summid][i].entries[0].isInactive;
								var rankcheck = [rGuild, rTier, rDIV, rLP, rWins, rLosses, rStreak, rVeteran, rBlood, rInactive];
							}
						break;
						case "RANKED_FLEX_SR":
							for(i=0;i<data[summid].length;i++){
								var rGuild = data[summid][i].name;
								var rTier = data[summid][i].tier;
								var rDIV = data[summid][i].entries[0].division;
								var rLP = data[summid][i].entries[0].leaguePoints;
								var rWins = data[summid][i].entries[0].wins;
								var rLosses = data[summid][i].entries[0].losses;
								var rStreak = data[summid][i].entries[0].isHotStreak;
								var rVeteran = data[summid][i].entries[0].isVeteran;
								var rBlood = data[summid][i].entries[0].isFreshBlood;
								var rInactive = data[summid][i].entries[0].isInactive;
								var rankcheck = [rGuild, rTier, rDIV, rLP, rWins, rLosses, rStreak, rVeteran, rBlood, rInactive];
							}
						break;
						default:
						break;
					console.log(rankcheck);
					summdata && summdata(rankcheck);
					}
				} catch (err) {
					console.log(err);
				}
			});
		}
	});
}

rankcheck.commands = {
//ADD COMMAND HERE
};
module.exports=rankcheck
